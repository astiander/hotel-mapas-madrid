function initMap() {
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 13,
    center: {lat: 43.316530, lng: -1.989161}
  });

  setMarkers(map);
}
var beaches = [
  ['Bondi Beach', 43.316530,  -1.989161, 5],
  ['Coogee Beach', 43.315927,  -1.997888, 4],
  ['Cronulla Beach',43.317597, -2.003757, 3],
  ['Manly Beach', 43.317793,-1.987051, 2],
  ['Maroubra Beach', 43.320103, -1.985367, 1]
];

function setMarkers(map) {
  var image = {url: 'img/map-marker.png'};
  var shape = {
    coords: [1, 1, 1, 20, 18, 20, 18, 1],
    type: 'poly'
  };
  for (var i = 0; i < beaches.length; i++) {
    var beach = beaches[i];
    var marker = new google.maps.Marker({
      position: {lat: beach[1], lng: beach[2]},
      map: map,
      icon: image,
      shape: shape,
      title: beach[0],
      zIndex: beach[3]
    });
  }
}

