
function initMap() {
    var datos = $.getJSON('data.json', function(data) {
      $.each(data.serviceList.service, function(i, item){
        $('#listaHoteles').append(`<option>${item.basicData.name}</option>`)
      });
      $('button').on('click', function(){
        selected = $('#listaHoteles').find(':selected').text();
        $.each(data.serviceList.service, function(i, item){
          if(item.basicData.name === selected){
            latitude = parseFloat(item.geoData.latitude);
            longitude = parseFloat(item.geoData.longitude);
          }
        })
          var map = new google.maps.Map($('#map'), {
          center: {lat: latitude, lng: longitude},
          scrollwheel: false,
          zoom: 8
        });
      })
      });
      
    
}
